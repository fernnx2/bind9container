FROM debian:buster-slim
LABEL maintainer="Fenando Menjivar <devfernando95@gmail.com>"
ENV NS=example.com
ENV IP_MASTER=192.168.0.1
ENV IP_SLAVE=192.168.0.2
ENV IP_NODE=192.168.0.3
ENV TYPE_DNS=""
ENV FORWARDERS=""

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends --no-install-suggests -y bind9 dnsutils iproute2 iputils-ping ipv6calc \
	&& chown bind:bind /var/cache/bind \
	&& chmod 0755 /var/cache/bind 


RUN mkdir -p /var/run/named /etc/bind/ && \
      chmod 775 /var/run/named && \
      chown root:bind /var/run/named > /dev/nul 2>&1


COPY ./docker-entrypoint.sh /sbin/docker-entrypoint.sh

RUN chmod 755 /sbin/docker-entrypoint.sh \
    && mkdir -p /var/run/named /etc/bind/ \
    &&  chmod 775 /var/run/named && \
    chown root:bind /var/run/named > /dev/nul 2>&1


EXPOSE 53/tcp 53/udp

ENTRYPOINT [ "/bin/bash","/sbin/docker-entrypoint.sh"]

