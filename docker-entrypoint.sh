#!/bin/bash
set -e

#nameserver
echo "nameserver ${IP_MASTER}
nameserver ${IP_SLAVE}
"search ${NS}"
nameserver 127.0.0.1
" > /etc/resolv.conf


cd /var/cache/bind/
touch bind.log
chmod 755 bind.log
cd /etc/bind/

chown -R root:bind /etc/bind /var/run/named
chown -R bind:bind /var/cache/bind
chmod -R 770 /var/cache/bind /var/run/named
chmod -R 750 /etc/bind

IP_ARPA=$(ipv6calc --in ipv4 --out revnibbles.arpa ${IP_MASTER})
IP_REV=$(echo ${IP_ARPA} | sed -e 's/.$//' | cut -c 3-) 



#creamos el archivo name.conf.logging y editamos su contenido
if [ ! -f /etc/bind/named.conf.logging ]
then
echo 'logging {
    category default {my_log; };
    channel my_log {
        file "/var/cache/bind/bind.log" versions 3 size 100k;
        print-time yes;
        print-severity yes;
        print-category yes;
    };

};' > named.conf.logging
fi
# editamos las opciones
echo "options {
    directory '"'/var/cache/bind'"';
    version '"'No disponible'"';
    listen-on port 53 {any; };
    forwarders {
               ${FORWARDERS}
        };
    recursion yes;
    allow-recursion {localnets; };
    allow-transfer {none; };
    dnssec-validation auto;
    listen-on-v6 {any; };
};" > named.conf.options



# Comprobando el tipo de servidor default node
if [ -z ${TYPE_DNS} ]
then
echo "Type default NODE"
else
#DNS Tipo MASTER
if [ ${TYPE_DNS} = "master" ]
then
echo "!!!-----Configurando DNS Maestro------!!!!"

#agregamos includes a named.conf
echo '
    include "/etc/bind/named.conf.acl"; 
    include "/etc/bind/named.conf.logging";
' >> named.conf

#creamos el archivo dns-secundarios y editamos contenido
if [ ! -f /etc/bind/named.conf.acl ]
then
echo "acl dns-secundarios {
        ${IP_SLAVE};
};" > named.conf.acl 
fi

#agregamos las zonas al archivo nammed.conf.local


echo "zone "'"'${NS}'"'" in{
    type master;
    file "'"/etc/bind/db.master.'${NS}'"'";
    allow-transfer {dns-secundarios; };
};
    zone "'"'${IP_REV}'"'" in {
        type master;
        file "'"/etc/bind/db.master.'${IP_REV}'"'";
        allow-transfer {dns-secundarios; };
};
" >> named.conf.local

#definimos la zona en el archivo 
if [ ! -f /etc/bind/db.master.${NS} ]
then
TTL='$TTL'
echo "
; BIND data file for ${NS}
$TTL    2d
@   IN  SOA ns1.${NS}. hostmaster.${NS}. (
                2   ; Serial
                12h ; Refresh
                15m ; Retry
                3w  ; Expire
                2h ) ; Negative Cache TTL
;
@   IN  NS  ns1.${NS}.
@   IN  NS  ns2.${NS}.

@   IN  A   ${IP_MASTER}
@   IN  A   ${IP_SLAVE}

@   IN  A   ${IP_NODE}

ns1 IN  A   ${IP_MASTER}
ns2 IN  A   ${IP_SLAVE}

node1   IN  A   ${IP_NODE}

" > db.master.${NS}
fi
#definimos la zona inversa en el archivo db.master.7.192.10.rev
if [ ! -f /etc/bind/db.master.${IP_REV} ]
then
echo "
; BIND data for file ${NS}
$TTL    2d
@   IN  SOA ns1.${NS}. hostmaster.${NS}. (
            1   ; Serial
            12h ; Refresh
            15m ; Retry
            3w  ; Expire
            2h )    ; Negative Cache TTL
;
@   IN  NS  ns1.${NS}.
@   IN  NS  ns2.${NS}.
@   IN  NS  ${NS}.

ns1 IN  A   ${IP_MASTER}
ns2 IN  A   ${IP_SLAVE}
node1   IN  A   ${IP_NODE}

${IP_MASTER##*.}  IN  PTR ns1.${NS}.
${IP_SLAVE##*.} IN  PTR ns2.${NS}.
${IP_NODE##*.}  IN  PTR nodo1.${NS}.
" > db.master.${IP_REV}
fi

#fin master
fi
if [ ${TYPE_DNS} = "slave" ]
then
echo "!!!-----Configurando DNS Slave------!!!!"

#agregamos las zonas esclavas al archivo named.conf.local
echo "zone "'"'${NS}'"' "in {
        type slave;
        file "'"/var/cache/bind/db.slave.'${NS}'"'";
        masters { ${IP_MASTER}; };
        allow-notify { ${IP_MASTER}; };
};

    zone "'"'${IP_REV}'"'" in {
        type slave;
        file "'"/var/cache/bind/db.slave.'${IP_REV}'"'";
        masters { ${IP_MASTER}; };
        allow-notify { ${IP_MASTER}; };
};
" >> named.conf.local
#fin slave
fi

#fin
fi


#comprobamos el estado de las zonas
named-checkconf -z

#verificamos el estado
if [ $? -eq 0 ];
then
    echo "named-checkconf ejecutado con exito!"
    #reiniciamos el servicio
    #exec /usr/sbin/named -g -c /etc/bind/named.conf -u bind
    exec /usr/sbin/named -u bind -f -4
else
    echo "Error en la comprobacion del comando named-checkconf el servicio bind no se inicio"
fi

