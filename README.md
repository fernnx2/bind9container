
 # BIND9 container

 #### Variables por defecto
    ENV NS=example.com
    ENV IP_MASTER=192.168.0.1
    ENV IP_SLAVE=192.168.0.2
    ENV IP_NODE=192.168.0.3
    ENV TYPE_DNS=""
    ENV FORWARDERS=""
Variables por defecto al crear el contenedor.


### Contruccion por defecto 1 servidor maestro, 1 esclavo y 1 nodo

    $sh build.sh


# Haciendo modificaciones segun sus necesidades 

## Creando la red

    docker network create --subnet=10.192.7.0/24 reddns

Si no se crea una red considerar su ip asignada al contenedor a la hora de crearlo.
El rango de red usted lo define al gusto

#### Definiendo un Servidor Maestro


    docker run --net=reddns --ip 10.192.7.2 -p 54:53/udp -e TYPE_DNS=master -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 --name master -it -d dnsserver

Se pueden omitir las variables IP_SLAVE y IP_NODE si no se definiran servidores esclavos y nodos.
Si no se creo la red, omitir --net de la siguiente manera

    docker run  -p 54:53/udp -e TYPE_DNS=master -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 --name master -it -d dnsserver

Considerar que IP_MASTER debe ir acorde a la ip asignada por defecto en el contenedor. Por lo general es la ip por defecto . 192.168.1.1

## Defieniendo un nodo esclavo

    docker run --net=reddns --ip 10.192.7.3 -p 56:53/udp -e TYPE_DNS=slave -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 -e IP_SLAVE=10.192.7.3 -e IP_NODE=10.192.7.10 --name slave -it -d dnsserver

En este caso las variables IP_MASTER y IP_SLAVE son obligatorias y el servidor maestro deberia estar creado anticipadamente. 
Considerar que las ip esten en concordancia

La variable IP_NODE puede omitirse si no se tendra ningun nodo

Si no se crea ninguna red , proceder de la misma manera al servidor maestro, explicado anteriormente.

## Definiendo un nodo

    docker run --net=reddns --ip 10.192.7.10 -p 57:53/udp -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 -e IP_SLAVE=10.192.7.3 -e IP_NODE=10.192.7.10 --name node1 -it -d dnsserver

Si no se creo la red , proceder de la misma manera explicado ateriormente.

Recordar mantener en concordancia las ip


## Solucion de problemas

  `devfernando95@gmail.com`