#!/bin/bash

docker network create --subnet=10.192.7.0/24 reddns

docker build  --no-cache -t "dnsserver" .

docker run --net=reddns --ip 10.192.7.2 -p 54:53/udp -e TYPE_DNS=master -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 -e IP_SLAVE=10.192.7.3 -e IP_NODE=10.192.7.10 --name master -it -d dnsserver

docker run --net=reddns --ip 10.192.7.3 -p 56:53/udp -e TYPE_DNS=slave -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 -e IP_SLAVE=10.192.7.3 -e IP_NODE=10.192.7.10 --name slave -it -d dnsserver

docker run --net=reddns --ip 10.192.7.10 -p 57:53/udp -e NS=pasteles.occ.ues.edu.sv -e IP_MASTER=10.192.7.2 -e IP_SLAVE=10.192.7.3 -e IP_NODE=10.192.7.10 --name node1 -it -d dnsserver